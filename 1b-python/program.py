#!/usr/bin/env python3

lines = [line.rstrip('\n') for line in open('indata')]
used = {0}
sum = 0

while True:
    for line in lines:
        num = int(line[1:])
        sum = sum + num if line[0] == '+' else sum - num

        if sum in used: 
            print(sum)
            exit(0)

        used.add(sum)

