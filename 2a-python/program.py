#!/usr/bin/env python3

lines = [line.rstrip('\n') for line in open('indata')]

# line -> groups of same letters -> group lengths 
# ans = sum(glenths == 2) * sum(glenths == 3)

def group_letters(line):
    freqs = {}
    for letter in line:
        if letter in freqs:
            freqs[letter] += 1
        else:
            freqs[letter] = 1

    return list(freqs.values())

groups = list(map(group_letters, lines))

def count(l):
    return sum(1 if any(x == l for x in g) else 0 
                for g in groups)

twos = count(2)
threes = count(3)

print(twos * threes)

