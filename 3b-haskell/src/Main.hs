module Main where

import qualified Data.List.Split as Split
import qualified Data.Set as Set

-- candidates = all claims
-- each claim c
--   c overlaps any previous looped?
--     yes -> candidates.remove(c + overlapped) 
--     no -> keep candidates as is 
--
-- foldl state: candidates + all previous looped claims 

loopClaims claims =
  remainingCids $ foldl loopBody (initState claims) claims 

loopBody candidates claim =
  let overlapped = getOverlapped claim (looped candidates)
  in if null overlapped 
     then appendLooped candidates claim
     else let cs = removeCandidates candidates (claim : overlapped) 
          in appendLooped cs claim

getOverlapped claim = filter (isClaimOverlap claim) 
  
isClaimOverlap claim1 claim2 =
  inRange (getX claim1) (getX2 claim1) (getX claim2) (getX2 claim2) &&
  inRange (getY claim1) (getY2 claim1) (getY claim2) (getY2 claim2)
   
inRange start1 end1 start2 end2 = end1 >= start2 && start1 <= end2

data Candidates = Candidates { 
  remainingCids :: Set.Set Int,
  looped :: [Claim]
} deriving (Show)

initState claims = Candidates cids []
  where cids = Set.fromList (map cid claims)

appendLooped candidates loopedClaim =
  candidates { looped = loopedClaim : looped candidates }

removeCandidates candidates claims =
  candidates { remainingCids = foldl (flip Set.delete) 
                 (remainingCids candidates) cidsToDelete }
  where cidsToDelete = map cid claims

data Claim = Claim { cid :: Int 
                   , getX :: Int
                   , getY :: Int
                   , getW :: Int
                   , getH :: Int  
                   } deriving (Show, Eq)

getX2 claim = getX claim + getW claim - 1

getY2 claim = getY claim + getH claim - 1

toClaim claimLine =  
  let n = filter (/="") $ Split.splitOneOf "# @,:x" claimLine
  in Claim (toInt n 0) (toInt n 1) 
           (toInt n 2) (toInt n 3) (toInt n 4)        
  where toInt n i = read (n!!i) :: Int

main :: IO ()
main = do
  indata <- readFile "indata"
  let claimLines = lines $! indata
  let claims = map toClaim claimLines
  let res = Set.elemAt 0 (loopClaims claims)
  print res 

