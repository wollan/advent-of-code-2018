module Main where

import qualified Data.List.Split as Split

-- each square fabric s 
--    claims = get overlapping claims (max 2)
--    if length claims >= 2
--    then ++1

allSquares = [(x,y) | x <- [0..1000], y <- [0..1000]]

howManySquares claims =
  let res = map (isOverlappingClaims claims) allSquares
  in (length . filter id) res

isOverlappingClaims claims p =
  isOverlap $ getOverlappingClaims (fst p) (snd p) claims
  where isOverlap [] = False
        isOverlap [x] = False
        isOverlap _ = True

getOverlappingClaims :: Int -> Int -> [Claim] -> [Claim]
getOverlappingClaims x y = filter (isOverlapping x y) 

isOverlapping x y claim = 
  x1 <= x && x < x2 && y1 <= y && y < y2
  where x1 = getX claim
        x2 = x1 + getW claim
        y1 = getY claim
        y2 = y1 + getH claim

data Claim = Claim { cid :: Int 
                   , getX :: Int
                   , getY :: Int
                   , getW :: Int
                   , getH :: Int  
                   } deriving (Show, Eq)

toClaim claimLine =  
  let n = filter (/="") $ Split.splitOneOf "# @,:x" claimLine
  in Claim (toInt n 0) (toInt n 1) 
           (toInt n 2) (toInt n 3) (toInt n 4)        
  where toInt n i = read (n!!i) :: Int

main :: IO ()
main = do
  indata <- readFile "indata"
  let claimLines = lines $! indata
  let claims = map toClaim claimLines
  let res = howManySquares claims 
  putStrLn $ "hello world: " ++ show res 



