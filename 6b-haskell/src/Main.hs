module Main where
import qualified Data.List as List
import qualified Data.List.Split as Split
import qualified Data.List.Unique as Unique

main ::IO ()
main = do
  indata <- readFile "indata"
  print $ run indata
  --putStrLn $ makeTable $ run indata
    
run indata =
  let coordLines = lines $! indata
      coords = map toCoord coordLines
      bareGrid = getGrid coords
      fullGrid = getGridLocs bareGrid coords 
      safeRegion = getLocsWithDistLessThan fullGrid 10000    
      safeRegionSize = length safeRegion
  in safeRegionSize

getLocsWithDistLessThan fullGrid maxDist = 
   filter (\GridLoc { gTotalDist = d } -> d < maxDist) fullGrid

getGridLocs bareGrid coords =
  map (\(x,y) -> GridLoc x y (getTotalDist x y coords)) bareGrid

getTotalDist x y coords =
  let distances = map (getDistance x y) coords
  in  List.sum distances

getDistance x y coord = 
  let dx = abs (x - cX coord)  
      dy = abs (y - cY coord)  
  in dy + dx

data GridLoc = GridLoc {
  gX :: Int,
  gY :: Int,
  gTotalDist :: Int 
} deriving (Show, Eq, Ord)

getGrid coords =
  let xs = map cX coords
      ys = map cY coords
      minX = List.minimum xs 
      maxX = List.maximum xs 
      minY = List.minimum ys 
      maxY = List.maximum ys 
  in [ (x,y) | x <- [minX..maxX], y <- [minY..maxY] ]

data Coord = Coord {
  cX :: Int,
  cY :: Int
} deriving (Show, Eq, Ord)

toCoord coordLine =
  let l = Split.splitOneOf ", " coordLine
  in Coord (toInt (head l)) (toInt (l!!2))  

makeTable xs = List.intercalate "\n" (map show xs)

toInt i = read i :: Int

