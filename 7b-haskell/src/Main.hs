module Main where

import qualified Debug.Trace as Trace
import qualified Data.Maybe as Maybe
import qualified Data.List as List
import qualified Data.List.Split as Split
import qualified Data.List.Unique as Unique
import qualified Data.Char as Char

main ::IO ()
main = do
  indata <- readFile "indata"
  print $ run indata
  --putStrLn $ makeTable $ run indata

run indata =
  let reqLines = lines $! indata 
      reqs = parseReqs reqLines 
      stepIds = getStepIds reqs
      steps = toSteps stepIds reqs 
      startSteps = findStartStep steps
      theStartStep = createTheStartStep startSteps 
      allSteps = theStartStep : steps
      workers = loopSteps allSteps createWorkers 0
      timeSheets = map workerToString workers
      timeItTook = List.maximum $ map length timeSheets
  in timeItTook 

workerToString Worker { wTimesheet = ts } = 
  map toChar ts

loopSteps :: [Step] -> [Worker] -> Int -> [Worker]
loopSteps steps workers time =
  let candidates = findCandidates steps  
      (newWorkers, newSteps) = scheduleWork candidates workers time steps
      (newTime, newNewWorkers, newNewSteps) = 
        forwardTime time newWorkers newSteps
  in if all isCompleted newNewSteps 
     then newNewWorkers 
     else loopSteps newNewSteps newNewWorkers newTime

scheduleWork candidates workers time steps =
  let availableWorkers = filter isWorkerAvailable workers
      workerCandidatePairs = zip availableWorkers candidates
      editedWorkers = map addStepToTimesheet workerCandidatePairs
      newWorkers = updateAll wId editedWorkers workers
      editedSteps = map (\(_,c) -> c { sStepStatus = Working }) 
                        workerCandidatePairs
      newSteps = updateAll sId editedSteps steps  
  in (newWorkers, newSteps)
  where 
    isWorkerAvailable worker =
      let futureTimeSheet = drop time $ wTimesheet worker
      in null futureTimeSheet  
    addStepToTimesheet (worker, candidate) =
      worker { wTimesheet = wTimesheet worker ++ 
                  replicate (timeForStep candidate) (sId candidate) }

-- until a step is done (time-wise)
forwardTime time workers steps = 
  let sheets = map (drop time . wTimesheet) workers  
      sheetLs = map (\s -> (length s, s)) sheets
      (minL, stepId:_) = findMinOn fst $ filter (\(l,_) -> l > 0) sheetLs
      newTime = time + minL
      newSteps = replaceWithWhen 
                  (\s -> s { sStepStatus = Completed })
                  (\s -> sId s == stepId)
                  steps
      wupdate w = if (null . drop time . wTimesheet) w  
                  then w { wTimesheet = wTimesheet w ++ 
                                          replicate minL (StepId '.') }
                  else w   
      newWorkers = map wupdate workers 
  in (newTime, newWorkers, newSteps) 

timeForStep Step { sId = (StepId id) } = 
  Char.ord id - 4 

data Worker = Worker {
  wTimesheet :: [StepId],
  wId :: Int
} deriving (Show)

createWorkers = map (Worker []) [0..4]

isCompleted step = sStepStatus step == Completed
isNotStarted step = sStepStatus step == NotStarted

findCandidates steps =
  let completedSteps = filter isCompleted steps
      nextIds = distinct $ completedSteps >>= sNexts  
      nextSteps = map idToStep nextIds  
      okPrereqSteps = filter prereqsAreOk $ 
                        filter isNotStarted nextSteps 
  in List.sortOn sId okPrereqSteps
  where
    idToStep id = Maybe.fromMaybe (createTheStartStep steps)
                    (List.find (\Step { sId = id' } -> id' == id) steps) 
    prereqsAreOk step = 
      let prereqs = sPrereqs step
          preReqSteps = map idToStep prereqs
      in all isCompleted preReqSteps 

createTheStartStep startSteps =
  Step (StepId '_') Completed [] (map sId startSteps) 

findStartStep =
  filter (null . sPrereqs)  

toSteps stepIds reqs =
  map (toStep reqs) stepIds

toStep reqs stepId =
  let prereqs = getSteps rTo rFrom
      nexts = getSteps rFrom rTo 
  in Step stepId NotStarted prereqs nexts
  where
    getSteps f g = map g $ filter (\r -> f r == stepId) reqs

getStepIds reqs =
  let allIdsInReqs = reqs >>= (\r -> [rFrom r, rTo r])
  in distinct allIdsInReqs   

data Step = Step {
  sId :: StepId,
  sStepStatus :: StepStatus,
  sPrereqs :: [StepId],
  sNexts :: [StepId]
} deriving (Show, Eq)

newtype StepId = StepId Char deriving (Show, Eq, Ord)

data StepStatus = NotStarted | Working | Completed 
  deriving (Show, Eq, Ord)  

toChar (StepId id) = id

data Req = Req {
  rFrom :: StepId,
  rTo :: StepId
} deriving (Show, Eq, Ord) 

parseReqs =  map parseReq 
   
parseReq reqLine =
  let l = Split.splitOneOf " " reqLine
  in Req { rFrom = StepId $ head $ l!!1, 
           rTo = StepId $ head $ l!!7 } 

makeTable xs = List.intercalate "\n" (map show xs)

distinct xs = map fst $ Unique.count xs

updateAll onKey updated = 
  map (\e -> Maybe.fromMaybe e 
              $ List.find (\d -> onKey e == onKey d) updated)

findMinOn key = List.minimumBy (\x y -> if key x < key y then LT else GT)

replaceWithWhen with when = map (\e -> if when e then with e else e)
