#!/usr/bin/env python3

lines = (line.rstrip('\n') for line in open('indata'))
sum = 0

for line in lines:
    num = int(line[1:])
    if line[0] == '+':
        sum += num
    else:
        sum -= num

print(sum)
