module Main where
import qualified Data.List as List
import qualified Data.List.Split as Split
import qualified Data.List.Unique as Unique

main ::IO ()
main = do
  indata <- readFile "indata"
  print $ run indata
  --putStrLn $ makeTable $ run indata
    
run indata =
  let coordLines = lines $! indata
      coords = map toCoord coordLines
      (bareGrid, edges) = getGrid coords
      fullGrid = getGridLocs bareGrid coords 
      infiniteCoords = getCoordsOnTheEdge fullGrid edges
      finiteCoords = coords List.\\ infiniteCoords
      finiteAreas = getAreas fullGrid finiteCoords 
      maxArea = List.maximum finiteAreas 
  in maxArea

getAreas fullGrid finiteCoords = 
  let locs = map (getLocsForCoord fullGrid) finiteCoords
  in  map length locs

getLocsForCoord fullGrid coord =
  filter isMatch fullGrid  
  where
    isMatch GridLoc { gClosest = Nothing } = False
    isMatch GridLoc { gClosest = Just c } = c == coord 

getCoordsOnTheEdge fullGrid (minX, maxX, minY, maxY) =
  let gridEdge = filter isEdge fullGrid 
      noMulti = filter skipMultiClose gridEdge  
      edgeCoords = map toClosest noMulti
  in distinct edgeCoords
  where 
    isEdge GridLoc { gX = x, gY = y, gClosest = c } =
      x == minX || x == maxX || y == minY || y == maxY
    skipMultiClose GridLoc { gClosest = Nothing } = False 
    skipMultiClose GridLoc { gClosest = Just _ } = True 
    toClosest GridLoc { gClosest = Just c } = c 

getGridLocs bareGrid coords =
  map (\(x,y) -> GridLoc x y (getClosestCoord x y coords)) bareGrid

getClosestCoord x y coords =
  let coordAndDist = map (getDistance x y) coords
      minDist = findMinOn snd coordAndDist 
      allWithMinDist = filter (\(_,d) -> d == snd minDist) coordAndDist
  in if lengthOne allWithMinDist 
     then Just $ fst minDist      
     else Nothing

getDistance x y coord = 
  let dx = abs (x - cX coord)  
      dy = abs (y - cY coord)  
      d = dy + dx
  in (coord, d)

data GridLoc = GridLoc {
  gX :: Int,
  gY :: Int,
  gClosest :: Maybe Coord
} deriving (Show, Eq, Ord)

getGrid coords =
  let xs = map cX coords
      ys = map cY coords
      minX = List.minimum xs 
      maxX = List.maximum xs 
      minY = List.minimum ys 
      maxY = List.maximum ys 
  in ([ (x,y) | x <- [minX..maxX], y <- [minY..maxY] ],
      (minX, maxX, minY, maxY)) 

data Coord = Coord {
  cX :: Int,
  cY :: Int
} deriving (Show, Eq, Ord)

toCoord coordLine =
  let l = Split.splitOneOf ", " coordLine
  in Coord (toInt (head l)) (toInt (l!!2))  

makeTable xs = List.intercalate "\n" (map show xs)

toInt i = read i :: Int

findMinOn key = List.minimumBy (\x y -> if key x < key y then LT else GT)

lengthOne [] = False
lengthOne [_] = True
lengthOne _ = False

distinct xs = map fst $ Unique.count xs
