module Main where

main :: IO ()
main = do
    content <- readFile "indata"
    let freqs = lines content
    let sum = foldl sumit 0 freqs
    print sum

sumit :: Integer -> String -> Integer
sumit sum ('+':f) = sum + toInt f
sumit sum ('-':f) = sum - toInt f

toInt i = read i :: Integer
