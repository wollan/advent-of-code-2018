module Main where
import Data.Char

processChar [] = []
processChar [c] = [c]
processChar (c1:c2:cs) =
  if shouldRemove c1 c2
  then processChar cs
  else c1 : processChar (c2:cs)

processLine line lastLength =
  if lastLength == length line
  then line
  else processLine (processChar line) (length line)

shouldRemove c1 c2 = abs (ord c1 - ord c2) == 32

skipUnitsOf unit = 
  filter (\c -> let d = abs (ord c - ord unit) in d /= 0 && d /= 32) 

skipThenReactLength line unit =
  length $ processLine (skipUnitsOf unit line) 0

minLength line = 
  (minimum . map (skipThenReactLength line)) ['a'..'z']

main = do
  indata <- readFile "indata"
  print $ minLength indata 
