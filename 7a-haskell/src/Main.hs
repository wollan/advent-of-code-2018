module Main where

import qualified Data.Maybe as Maybe
import qualified Data.List as List
import qualified Data.List.Split as Split
import qualified Data.List.Unique as Unique

main ::IO ()
main = do
  indata <- readFile "indata"
  print $ run indata
  --putStrLn $ makeTable $ run indata

run indata =
  let reqLines = lines $! indata 
      reqs = parseReqs reqLines 
      stepIds = getStepIds reqs
      steps = toSteps stepIds reqs 
      startSteps = findStartStep steps
      theStartStep = createTheStartStep startSteps 
      allSteps = theStartStep : steps
      resultSteps = loopSteps allSteps
  in map toChar resultSteps 

loopSteps :: [Step] -> [StepId]
loopSteps steps =
  let candidates = findCandidates steps  
      bestCandidate = selectBestCandidate candidates
  in if isDone bestCandidate
     then [sId bestCandidate]
     else let newSteps = markAsCompleted bestCandidate steps
          in sId bestCandidate : loopSteps newSteps

isDone bestCandidate =
  null $ sNexts bestCandidate  

markAsCompleted bestCandidate = 
  replaceWithWhen (\s -> s { sIsCompleted = True }) 
                  (\s -> sId s == sId bestCandidate)

selectBestCandidate candidates =
  head $ List.sortOn sId candidates

findCandidates steps =
  let completedSteps = filter sIsCompleted steps
      nextIds = distinct $ completedSteps >>= sNexts  
      nextSteps = map idToStep nextIds  
      okPrereqSteps = filter prereqsAreOk $ 
                        filter (not . sIsCompleted) nextSteps 
  in okPrereqSteps
  where
    idToStep id = Maybe.fromMaybe (createTheStartStep steps)
                    (List.find (\Step { sId = id' } -> id' == id) steps) 
    prereqsAreOk step = 
      let prereqs = sPrereqs step
          preReqSteps = map idToStep prereqs
      in all sIsCompleted preReqSteps 

createTheStartStep startSteps =
  Step (StepId '_') True [] (map sId startSteps) 

findStartStep =
  filter (null . sPrereqs)  

toSteps stepIds reqs =
  map (toStep reqs) stepIds

toStep reqs stepId =
  let prereqs = getSteps rTo rFrom
      nexts = getSteps rFrom rTo 
  in Step stepId False prereqs nexts
  where
    getSteps f g = map g $ filter (\r -> f r == stepId) reqs

getStepIds reqs =
  let allIdsInReqs = reqs >>= (\r -> [rFrom r, rTo r])
  in distinct allIdsInReqs   

data Step = Step {
  sId :: StepId,
  sIsCompleted :: Bool,
  sPrereqs :: [StepId],
  sNexts :: [StepId]
} deriving (Show, Eq)

newtype StepId = StepId Char deriving (Show, Eq, Ord)

toChar (StepId id) = id

data Req = Req {
  rFrom :: StepId,
  rTo :: StepId
} deriving (Show, Eq, Ord) 

parseReqs =  map parseReq 
   
parseReq reqLine =
  let l = Split.splitOneOf " " reqLine
  in Req { rFrom = StepId $ head $ l!!1, 
           rTo = StepId $ head $ l!!7 } 

makeTable xs = List.intercalate "\n" (map show xs)

distinct xs = map fst $ Unique.count xs

replaceWithWhen with when = map (\e -> if when e then with e else e) 
