#!/usr/bin/env python3

lines = [line.rstrip('\n') for line in open('indata')]

for id1 in lines:
    for id2 in lines:
        same_char_indices = [] 
        for i in range(len(id1)):
            if id1[i] == id2[i]:
                same_char_indices.append(i)

        if len(same_char_indices) == len(id1) - 1:
            print("".join(map(lambda i: id1[i], same_char_indices)))
            exit(0)

