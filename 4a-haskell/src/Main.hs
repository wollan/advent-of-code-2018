module Main where

import qualified Data.List as List
import qualified Data.List.Split as Split

-- indata: sleep record lines
-- outdata: guard w/ most sleeptime + his worst minute
-- gid = guard id 
-- 
-- -1. sort record lines 
-- 0. create sleep records (day + gid + minute + (wakeup | fallasleep)) 
-- 1. group records to day sheet (gid + sleeparray[60])
--    sleeparray = [0, 1, ...] where 0 is awake, 1 asleep
-- 2. group day sheets by id into a guard sheet (gid + sleeparray[60])
--    sleeparray2 = [0, 32, 9, 1, ...] with total minutes asleep per guard
-- 3. find gid with max(sum(sleeparray2))  
-- 4. calc which minute max(sleeparray2) occurs for that gid
-- 5. multiply result from 3. and 4. 

main ::IO ()
main = do
  indata <- readFile "indata"
  print $ run indata

run indata = 
  let recordLines = lines $! indata
      sortedRecordLines = List.sort recordLines
      sleepRecords = toSleepRecords sortedRecordLines
      daySheets = toDaySheets sleepRecords 
      guardSheets = toGuardSheets daySheets 
      sleeper = guardSheetWithMostTotalSleep guardSheets
      sleepMinute = mostAsleepMinute $ fst sleeper
      sleepyGid = sGid $ fst sleeper 
  in sleepMinute * sleepyGid 

mostAsleepMinute sleeper =
  fst $ findMaxOn snd $ zip [0..] $ sSleepArray sleeper

guardSheetWithMostTotalSleep guardSheets =
  let totals = map (\g -> (g, (sum . sSleepArray) g)) guardSheets
  in findMaxOn snd totals

toGuardSheets daySheets =
  map toGuardSheet $ groupOn sGid daySheets 
  where 
    toGuardSheet daySheetGroup =
      Sheet {
        sGid = sGid $ head daySheetGroup,
        sSleepArray = foldl1 (zipWith (+)) $ map sSleepArray daySheetGroup
      }  

toDaySheets sleepRecords =
  let gs = List.groupBy (\r1 r2 -> srDay r1 == srDay r2) sleepRecords
  in map toDaySheet gs
  where 
    toDaySheet recordGroup =
      let srs = recordGroup ++ [SleepRecord 0 0 60 FallAsleep]
          sleeparray = snd $ foldl foldbody (0, []) srs
          gid = srGid $ head recordGroup 
      in Sheet gid sleeparray 
      where 
        foldbody (skip, arr) e = 
          let m = srMinute e 
          in (m, arr ++ replicate (m - skip) (toSleepMinData e))

data Sheet = Sheet {
  sGid :: Int,
  sSleepArray :: [Int]
} deriving (Show)

toSleepRecords recordLines =
  let (_, _, records) = foldl body (-1, -1, []) recordLines
  in reverse records
  where 
    body (day, gid, records) recordLine =
      if List.isSubsequenceOf "begin shift" recordLine
      then (day + 1, 
            read (Split.splitOneOf "# " recordLine !! 4) :: Int,
            records)    
      else (day, gid, toSleepRecord day gid recordLine : records) 

data SleepRecord = SleepRecord {
  srDay :: Int,
  srGid :: Int,
  srMinute :: Int,
  srSleepEvent :: SleepEvent 
} deriving (Show)

data SleepEvent = Wakeup | FallAsleep 
  deriving (Show)

toSleepRecord day gid recordLine =
  SleepRecord day gid minute (toSleepEvent recordLine)
  where
    minute = read (Split.splitOneOf ":]" recordLine !! 1) :: Int
 
toSleepEvent recordLine =
  if List.isSubsequenceOf "wakes up" recordLine
  then Wakeup
  else FallAsleep
 
toSleepMinData SleepRecord { srSleepEvent = Wakeup } = 1
toSleepMinData SleepRecord { srSleepEvent = FallAsleep } = 0

groupOn :: Ord b => (t -> b) -> [t] -> [[t]]
groupOn key list = 
  List.groupBy (\e d -> key e == key d) $ List.sortOn key list

findMaxOn key = List.maximumBy (\x y -> if key x < key y then LT else GT)

makeTable xs = List.intercalate "\n" (map show xs)

